<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Task;
use Illuminate\Http\Request;

// Route::get('/', function () {
//     return view('welcome');
// });

/**
 * Show Task Dashboard
 */
// Route::get('/', function () {
//     $tasks = Task::orderBy('created_at', 'asc')->get();

//     return view('tasks', [
//         'tasks' => $tasks
//     ]);
// });

// /**
//  * Add New Task
//  */
// Route::post('/task', function (Request $request) {
//     $validator = Validator::make($request->all(), [
//         'name' => 'required|max:255',
//     ]);

//     if ($validator->fails()) {
//         return redirect('/')
//             ->withInput()
//             ->withErrors($validator);
//     }

//     $task = new Task;
//     $task->name = $request->name;
//     $task->save();

//     return redirect('/');
// });

// /**
//  * Delete Task
//  */
// Route::delete('/task/{task}', function (Task $task) {
//     $task->delete();

//     return redirect('/');
// });
Route::model('tasks', 'Task');
Route::model('projects', 'Project');

Route::bind('tasks', function($value, $route) {
    return App\Task::whereSlug($value)->first();
});
Route::bind('projects', function($value, $route) {
    return App\Project::whereSlug($value)->first();
});

Route::get('/',function(){
    return "hello";
});

Route::resource('projects.tasks', 'TasksController');
Route::resource('projects', 'ProjectsController');